window.onload = function () {
    //获取小图容器
    var bigImg = document.querySelector('section .content .leftArea .bigImg-wrap .bigImg ');
    //获取放大器外层容器
    var bigImgWrap = document.querySelector('section .content .leftArea .bigImg-wrap ');
    //初始化蒙版
    var maskNode = null;
    //初始化大图容器
    var bigImgBoxNode = null;
    //初始化大图
    var bigImgNode = null;
    //模拟动态数据
    var imgArray = [
        {b: "./img/b1.png", s: "./img/s1.png"},
        {b: "./img/b2.png", s: "./img/s2.png"},
        {b: "./img/b3.png", s: "./img/s3.png"},
        {b: "./img/b1.png", s: "./img/s1.png"},
        {b: "./img/b2.png", s: "./img/s2.png"},
        {b: "./img/b3.png", s: "./img/s3.png"},
        {b: "./img/b1.png", s: "./img/s1.png"},
        {b: "./img/b2.png", s: "./img/s2.png"}
    ];
    //动态图索引
    var index = 0;

//鼠标移入事件
    bigImgWrap.onmouseenter = function () {
        //当鼠标移入元素,创建蒙版,大图,大图容器 如果蒙版不存 即可创建,如果存在 不创建
        if (!maskNode) {
            maskNode = document.createElement('div');
            //结合css当中已存在的class 对蒙版设置样式
            maskNode.className = 'mask';
            //把蒙版插入到小图容器当中
            bigImg.appendChild(maskNode);
            //创建大图容器
            bigImgBoxNode = document.createElement('div');
            bigImgBoxNode.className = 'bigImgBox';
            //创建大图
            bigImgNode = document.createElement('img');
            bigImgNode.src = 'img/b1.png';
            //把大图放到大图容器中
            bigImgBoxNode.appendChild(bigImgNode);
            //把大图容器添加到放大镜结构中
            bigImgWrap.appendChild(bigImgBoxNode);
            //同步大图
            bigImgNode.src = imgArray[index].b;
        }
        bigImg.onmousemove = function (event) {
            //移动的逻辑
            var maskPosition = {
                left: event.clientX - maskNode.offsetWidth / 2 - bigImg.getBoundingClientRect().left,
                top: event.clientY - maskNode.offsetHeight / 2 - bigImg.getBoundingClientRect().top
            }
            //判断边界
            if (maskPosition.left <= 0) {
                maskPosition.left = 0;
            } else if (maskPosition.left >= bigImg.clientWidth - maskNode.offsetWidth) {
                maskPosition.left = bigImg.clientWidth - maskNode.offsetWidth
            }

            if (maskPosition.top <= 0) {
                maskPosition.top = 0;
            } else if (maskPosition.top >= bigImg.clientHeight - maskNode.offsetHeight) {
                maskPosition.top = bigImg.clientHeight - maskNode.offsetHeight;
            }
            //对蒙版进行赋值
            maskNode.style.left = maskPosition.left + 'px';
            maskNode.style.top = maskPosition.top + 'px';

            //计算比例
            var scale = (bigImg.clientWidth - maskNode.offsetWidth) / (bigImgNode.clientWidth - bigImgBoxNode.offsetWidth);
            //计算大图偏移

            var bigImgPosition = {
                left: maskPosition.left / scale,
                top: maskPosition.top / scale
            }
            bigImgNode.style.marginLeft = -bigImgPosition.left + 'px';
            bigImgNode.style.marginTop = -bigImgPosition.top + 'px';
        }


    }
    bigImgWrap.addEventListener('mouseleave', function () {
        //移出删除所有新增的节点
        bigImg.removeChild(maskNode);
        bigImgWrap.removeChild(bigImgBoxNode);

        maskNode = null;
        //初始化大图容器
        bigImgBoxNode = null;
        //初始化大图
        bigImgNode = null;

        //事件解绑
        bigImg.onmousemove = null;
        bigImgWrap.onmouseleave = null;
    })

    //获取非行内样式
    function getStyle(obj, attr) {
        //第一个参数为要获取的元素,第二个参数为要获取的样式
        if (obj.currentStyle) {
            return obj.currentStyle(attr);
        } else {
            return window.getComputedStyle(obj)[attr];
        }
    }

    //缩略图滑动效果
    thumbnail();

    function thumbnail() {
        //获取左右按钮
        var prev = document.querySelector('section .content .leftArea .imgScroll .prev ');
        var next = document.querySelector('section .content .leftArea .imgScroll .next ');
        //获取图片容器(ul)
        var imgContainer = document.querySelector('section .content .leftArea .imgScroll .listImgWrap .listImg');
        //获取li
        var liNodes = document.querySelectorAll('section .content .leftArea .imgScroll .listImgWrap .listImg li');
        //定义每次滑动的数量
        var moveNum = 2;
        //定义容器中默认显示id数量
        var showNum = 5;

        //计算总共可以移动的长度 (li总数 - 默认显示li的个数)*(li的宽度+li的marginRight)
        var canLength = (liNodes.length - showNum) * (liNodes[0].offsetWidth + parseInt(getStyle(liNodes[0], 'marginRight')));
        //计算每次移动的长度     (一个元素的宽度(带margin))*每次移动的数量
        var moveLength = (liNodes[0].offsetWidth + parseInt(getStyle(liNodes[0], 'marginRight'))) * moveNum;
        //初始化已经移动的距离
        var alreadyLength = 0;


        next.onclick = function () {
            //如果已经走过的距离 小于总共可以走的距离 才能继续移动
            if (alreadyLength < canLength) {
                /*  剩余距离  大于每次可以移动的长度   则每次都该走的距离
                    剩余的距离 = 总共可以走的距离 - 已经移动的距离*/
                if (canLength - alreadyLength > moveLength) {
                    alreadyLength += moveLength;
                    imgContainer.style.left = -alreadyLength + 'px';
                } else {
                    //剩余距离小于每次可以移动的距离  则直接走完剩余的距离
                    alreadyLength += (canLength - alreadyLength);
                    imgContainer.style.left = -alreadyLength + 'px';
                }
            }
        };
        prev.onclick = function () {
            //相对于prev来说   可以动的距离 就是alreadyLength
            if (alreadyLength > 0) {
                //如果可以走的距离 大于每次移动的距离  则走该走的长度  否则直接为0
                if (alreadyLength > moveLength) {
                    alreadyLength -= moveLength;
                    imgContainer.style.left = -alreadyLength + 'px';
                } else {
                    alreadyLength = 0;
                    imgContainer.style.left = alreadyLength + 'px';
                }
            }
        };
    }

    //点击缩略图,小图跟着切换
    thumbnailClick();

    function thumbnailClick() {
        //获取缩略图
        var thumbnail = document.querySelectorAll('section .content .leftArea .imgScroll .listImgWrap .listImg li img');
        //获取小图
        var smallImg = document.querySelector('section .content .leftArea .bigImg-wrap .bigImg img ');


        for (let i = 0; i < thumbnail.length; i++) {
            //给每个缩略图添加单击事件

            //设置索引
            // var num=i;
            thumbnail[i].onclick = function () {
                index = i;
                smallImg.src = this.src;

            }
        }


    }

    //商品内容筛选  动态生成
    screening();

    function screening() {
        //模拟数据
        var crumbData = goodData.goodsDetail.crumbData;
        //获取容器元素
        var selectAreaNode = document.querySelector('section .content .rightArea .selectArea');
        crumbData.forEach(function (item) {
            //每次遍历进来 先创建一个dl标签
            var dlNode = document.createElement('dl');
            //每个dl当中都只有一个dt 所以这里同步创建即可
            var dtNode = document.createElement('dt');
            //通过title属性   给每一个dt设置文本
            dtNode.innerHTML = item.title;
            //把dt插入到dl中
            dlNode.appendChild(dtNode);

            //遍历每一个对象当中data属性    动态创建dd
            item.data.forEach(function (item) {
                //生成dd
                var ddNode = document.createElement('dd');
                //给dd插入内容
                ddNode.innerHTML = item.type;
                //给生成的dd添加自定义属性 保存对价格的修改
                ddNode.setAttribute('changePrice', item.changePrice);


                //把dd插入到dl中
                dlNode.appendChild(ddNode);
            });
            //把当前遍历生成的dl插入到页面中
            selectAreaNode.appendChild(dlNode);
        });
        //以下是点击筛选内容的交互
        //获取dl集合
        var dlList = selectAreaNode.getElementsByTagName('dl');
        //定义一个数组,数组中保存筛选条件,需要的时候  按顺序保存和获取里面的内容
        //并且  保证数组中只有dlList.length(crumbData.length)个值 当更换数据时  直接替换原有位置即可
        var arr = new Array(crumbData.length);
        //填充数组   用传入的值填充整个数组
        arr.fill(0);
        console.log(arr);
        // var arr=[];
        // for(var i=0;i<crumbData.length;i++){
        //     arr.push[0];
        // }

        //遍历所有的dl标签 然后给当前的dl标签中的dd绑定事件
        for (var i = 0; i < dlList.length; i++) {
            //给每个dl添加索引
            dlList[i].index = i;
            //获取当前dl标签中所有的dd
            (function () {
                var ddList = dlList[i].getElementsByTagName('dd');
                //对所有的dd绑定单击事件
                for (var j = 0; j < ddList.length; j++) {
                    ddList[j].onclick = function () {
                        //dd高亮切换
                        //先恢复所有dd的默认颜色
                        for (var i = 0; i < ddList.length; i++) {
                            ddList[i].style.color = '#666';
                        }
                        this.style.color = 'red';
                        //用户点击了dd 根据当前的dd知道对应的dl获取索引
                        arr[this.parentNode.index] = this;
                        //更新了新的选择结果   重新计算价钱
                        priceSum(arr);
                        console.log(arr);
                        //获取存放选择结果的容器
                        var choosedNode = document.querySelector('section .content .rightArea .selectArea .choosed');
                        //每次创建元素之前先把  结果容器清空
                        choosedNode.innerHTML = '';
                        //遍历数组  生成对应的选择结果
                        arr.forEach(function (item, index) {
                            //检测数组,当前元素不为0的时候   创建mark标签
                            if (item) {
                                //创建mark标签
                                var markNode = document.createElement('mark');
                                //把数组的内容放到mark标签中
                                markNode.innerHTML = item.innerHTML;
                                //创建mark标签中的a标签
                                var aNode = document.createElement('a');
                                //创建a标签时用一个自定义属性保存当前a所在数组的下标
                                //setAttribute用于设置元素自定义属性 参数1 属性名称 参数2 要设置的属性值
                                //getAttribute用于获取元素自定义属性
                                aNode.setAttribute('num', index);
                                aNode.innerHTML = 'X';
                                //把a标签放入到mark中
                                markNode.appendChild(aNode);
                                //把mark标签放入到存放选择结果的容器中
                                choosedNode.appendChild(markNode);
                            }
                        });
                        //获取mark标签所有的a标签
                        var aList = choosedNode.querySelectorAll('mark>a');
                        for (var i = 0; i < aList.length; i++) {
                            aList[i].onclick = function () {
                                //获取当前点击的a标签的索引     去操作对应的dl元素当中dd高亮
                                var num = parseInt(this.getAttribute('num'));
                                //删除掉当前点击的mark标签
                                this.parentNode.parentNode.removeChild(this.parentNode);
                                var ddList = dlList[num].querySelectorAll('dd');
                                for (var i = 0; i < ddList.length; i++) {
                                    ddList[i].style.color = '#666';
                                }
                                //把默认颜色给加上
                                ddList[0].style.color = 'red';
                                //每一次删除一个内容的时候,要把当前数组当中对应的元素清空
                                arr[num] = 0;
                                priceSum(arr);

                            }
                        }
                    }
                }
            })();
        }
        //计算价钱的函数
        //只要保存筛选条件的数组被操作了  则调用该函数  所以可以把数组当做传输传递过来
        function priceSum(arr) {
            var newPrice = goodData.goodsDetail.price;

            //遍历arr数组  然后把里边每一个dd的changePrice加等
            arr.forEach(function (item) {
                //先判断当前元素  是否有值
                if (item) {
                    newPrice += parseInt(item.getAttribute('changePrice'))
                }
            });
            console.log(newPrice)
            //获取价钱容器
            var priceNode = document.querySelector('section .content .rightArea .price-wrap .priceArea1 .price02 span');
            priceNode.innerHTML = newPrice;
            //只要计算价钱 就改变选择搭配的价钱
            var choosePriceNode = document.querySelector('.product .product-right .choose .mainContent .master p');


        }
    }
    //选择搭配价钱
    fittingPrice();

    function fittingPrice() {
        //获取搭配后的总价元素
        var chooseAllPrice = document.querySelector('.product .product-right .choose .mainContent .result .price');
        //获取搭配区域所有的复选框
        var chooseAllCheckBox = document.querySelectorAll('.product .product-right .choose .mainContent .suits .suitsItem input');
        //给所有的复选框 绑定单击绑定事件
        for (var i = 0; i < chooseAllCheckBox.length; i++) {
            chooseAllCheckBox[i].onclick = function () {
                //获取原价元素
                var choosePriceNode = document.querySelector('.product .product-right .choose .mainContent .master p');
                //获取原价
                var choosePrice = parseInt(choosePriceNode.innerHTML.substr(1));
                //遍历所有的复选框 判断哪一个被选中
                chooseAllCheckBox.forEach(function (item) {
                    //如果当前的多选框被选中 则返回的checked属性为true
                    if (item.checked) {
                        choosePrice += parseInt(item.value);
                    }
                })
                chooseAllPrice.innerHTML = '¥' + choosePrice;
            }
        }
    }

    //路径导航动态生成
    pathUrl();

    function pathUrl() {
        //获取容器
        var conPoinNode = document.querySelector('section .conpin');
        //获取数据
        var path = goodData.path;
        //遍历路径的数据
        path.forEach(function (item, index) {
            //遍历路径的数据
            var aNode = document.createElement('a');
            //先判断数据有没有url属性
            if (index == path.length - 1) {
                //最后一个a标签   没有href属性    也不需要创建i标签
                aNode.innerHTML = item.title;
                conPoinNode.appendChild(aNode);
            } else {
                aNode.href = item.url;
                aNode.innerHTML = item.title;
                conPoinNode.appendChild(aNode);
                var iNode = document.createElement('i');
                iNode.innerHTML = '/';
                conPoinNode.appendChild(iNode);
            }
        })
    }

    //商品详情的数据动态生成
    goodsDetail();

    function goodsDetail() {
        //获取容器
        var info1Node = document.querySelector('section .content .rightArea .phoneText');
        //获取数据
        var infoData = goodData.goodsDetail;
        //字符串拼接
        var info1Content = `<h3>Apple iPhone 6s（A1700）64G玫瑰金色 移动通信电信4G手机</h3>
                    <p>推荐选择下方[移动优惠购],手机套餐齐搞定,不用换号,每月还有花费返</p>
                </div>
                <div class="price-wrap">
                    <div class="priceArea1">
                        <div class="f"></div>
                        <div class="price">
                            <p>价&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格</p>
                        </div>
                        <div class="price02">
                            <i>¥</i>
                            <span>${infoData.price}</span>
                            <em>降价通知</em>
                        </div>
                        <div class="appraise ">
                            <em>累计评价：</em>
                            <i>3283</i>
                        </div>
                    </div>
                    <div class="priceArea2">
                        <div class="title">促&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;销</div>
                        <div class="text">
                            <p><i>${infoData.promoteSales.type}</i>${infoData.promoteSales.content}</p>
                        </div>
                    </div>`;
        info1Node.innerHTML = info1Content;

    }

    //选项卡封装
    function Tab(btns, content) {
        this.tabBtn = btns;
        this.content = content;
        //保存当前的this,用于内部事件的回调函数访问外部的this
        var _this = this;

        //给所有的btn绑定单击事件
        for (var i = 0; i < this.tabBtn.length; i++) {
            //需要给所有的按钮添加索引
            this.tabBtn[i].index = i;
            this.tabBtn[i].onclick = function () {
                //this  事件源
                _this.clickBtn(this)
            }
        }
    }

    Tab.prototype.clickBtn = function (btn) {
        //btn接受的就是当前点击的事件源
        //选项卡切换
        for (var i = 0; i < this.tabBtn.length; i++) {
            this.tabBtn[i].className = '';
            this.content[i].style.display = 'none';
        }
        btn.className = 'active';
        this.content[btn.index].style.display = 'block';
    }
    //侧边栏调用tab切换
    asideNav();

    function asideNav() {
        var tabNodes = document.querySelectorAll('.product > aside .tabWrap h4');
        var contentNodes = document.querySelectorAll('.product > aside .tabContent .tabItem');
        new Tab(tabNodes, contentNodes);
    }

    //评论区域tap切换
    intro();

    function intro() {
        var tabNodes = document.querySelectorAll('.product .product-right .intro .tabWrap li');
        var contentNodes = document.querySelectorAll('.product .product-right .intro .tabContent .tabItem');
        new Tab(tabNodes, contentNodes);
    }




}
/*
1实例的隐式原型指向构造函数的显示原型对象
2原型对象的constructor属性指向构造函数
3原型对象是Object()构造函数的实例
4原型对象的隐式原型指向Object构造函数的显示原型对象
5Object构造函数的显示原型对象可以理解为原型链的终点它的隐式原型为null
6所有的函数都是Function()构造函数的实例包括Function()和Object()
7所有函数的隐式原型都指向Function()构造函数的显示原型对象
8Function()构造函数的隐式原型对象和显示原型对象是同一个
9Function()的显示原型对象也是Object()的实例
10总之沿原型链查找最终都会找到Object()构造函数的显示原型对象
* */
